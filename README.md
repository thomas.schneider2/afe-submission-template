# Template für Abgaben

In diesem Repository finden Sie eine bespielhafte Abgabe für ein imaginäres Aufgabenblatt -1.
Es soll Ihnen einige LaTeX-Befehle demonstrieren, die für die ersten Übungsblätter wichtig sein werden.
Dennoch können wir Sie nur dazu ermutigen, sich über dieses Starterpaket hinaus mit LaTeX zu beschäftigen oder mit dem vorhandenen Template herumzuspielen.
Ihr zukünftiges Ich wird es Ihnen danken.

An dieser Stelle möchten wir auch noch einmal auf das vom HRZ aufgesetzte [ShareLaTeX-Instanz](https://www.hrz.tu-darmstadt.de/services/it_services/sharelatex_hrz/index.de.jsp) verweisen.
Die Nutzung der ShareLaTeX-Instanz ist für die Zusammenarbeit in einer Abgabegruppe vorteilhaft, da LaTeX in Kooperation mit unterschiedlichen OS und Installationen zu einer großen Variation von Fehlern bzw. Problemen führen kann.

Um diese Template in ShareLaTeX zu nutzen, laden Sie sich das Repository als Zip-Ordner herunter und wählen Sie in ShareLaTeX unter "New Project" die Option "Upload Projekt" aus.
Hier können Sie den Zip-Ordner hochladen und bekommen somit eine Vorlage, sodass Sie sofort los texen können.

Wir wünschen gutes Gelingen und viel Spaß.<br>
Viele Grüße<br>
Thomas Schneider

-----

_Anmerkung:_ Das Logo des Git-Projektes stammt von [Wikipedia](https://de.m.wikipedia.org/wiki/Datei:LaTeX_logo.svg).
